﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoolfInput : MonoBehaviour, IDestinationInput, IMoveSpeedInput
{
    private GameObject m_target;
    private Transform m_characterTransform;

    [SerializeField]
    private float m_moveSpeed = 5;

    private ICharacterPlatformStatus m_targetPlatformStatus;

    public Vector3 DestinationInput { get; set; }
    public float MoveSpeed => m_moveSpeed;

    // Start is called before the first frame update
    void Start()
    {
        m_characterTransform = transform.parent;
    }

    void Awake()
    {
        FindTarget();
    }

    private void FindTarget()
    {
        m_target = Helper.FindTarget();
        m_targetPlatformStatus = m_target?.GetComponent<ICharacterPlatformStatus>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_target == null || m_target.Equals(null))
            FindTarget();

        if (m_target != null && !m_target.Equals(null))
        {
            if (m_targetPlatformStatus.IsOnPlatform)
            {
                m_target = null;
                DestinationInput = m_characterTransform.position;
            }
            else
            {
                DestinationInput = m_target.transform.position;
                if (Vector3.Distance(m_characterTransform.position, DestinationInput) < 1.2f)
                {
                    GameState.Instance.KillObject(m_target);
                    FindTarget();
                }
            }        }
        else
        {
            DestinationInput = m_characterTransform.position;
        }
    }
}