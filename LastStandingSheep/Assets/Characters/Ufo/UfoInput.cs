﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public enum UfoBotState
{
    SearchTarget,
    Patrol,
    Abduction,
}

public class UfoInput : MonoBehaviour, IDestinationInput, IMoveSpeedInput, IAbductionState
{
    private GameObject m_target;
    private Transform m_characterTransform;
    private NavMeshAgent m_agent;

    [SerializeField]
    private float m_moveSpeed = 5;
    [SerializeField]
    private float m_patrolTime = 3.0f;

    private UfoBotState m_state;
    private float m_startPatrolTime;

    public Vector3 DestinationInput { get; set; }
    public float MoveSpeed => m_moveSpeed;

    public bool IsAbductionInProcess => m_state == UfoBotState.Abduction;

    
    // Start is called before the first frame update
    void Start()
    {
        m_characterTransform = transform.parent;
        m_agent = m_characterTransform.GetComponent<NavMeshAgent>();
    }

    void Awake()
    {
        m_target = Helper.FindTarget();
    }

    // Update is called once per frame
    void Update()
    {
        switch (m_state)
        {
            case UfoBotState.SearchTarget:
                SearchTarget();
                break;
            case UfoBotState.Patrol:
                Patrol();
                break;
            case UfoBotState.Abduction:
                Abduction();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void Abduction()
    {
        DestinationInput = m_characterTransform.position;
    }

    private void Patrol()
    {
        if (Time.time - m_startPatrolTime < m_patrolTime)
        {
            if (Helper.IsAgentCompleteMove(m_agent))
            {
                DestinationInput = Helper.GetRandomPosition(GameConstants.BotAppearRadius);
            }
        }
        else
        {
            m_state = UfoBotState.SearchTarget;
        }
    }

    private void SearchTarget()
    {
        if (m_target == null || m_target.Equals(null))
            m_target = Helper.FindTarget();

        if (m_target != null && !m_target.Equals(null))
        {
            DestinationInput = m_target.transform.position;

            var vec = m_characterTransform.position - DestinationInput;
            vec.y = 0;
            if (vec.magnitude < 2.0f)
            {
                var completable = GameState.Instance.UfoAbductObject(m_characterTransform.gameObject, m_target);
                completable.OnComplete += OnComplete;

                m_agent.ResetPath();
                DestinationInput = m_characterTransform.position;
                m_state = UfoBotState.Abduction;
            }
        }
        else
        {
            DestinationInput = m_characterTransform.position;
        }
    }

    private void OnComplete(GameObject obj)
    {
        m_state = UfoBotState.Patrol;
        DestinationInput = Helper.GetRandomPosition(GameConstants.BotAppearRadius);
        m_startPatrolTime = Time.time;
    }
}