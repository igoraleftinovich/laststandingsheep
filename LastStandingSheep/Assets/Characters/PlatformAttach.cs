﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlatformAttach : MonoBehaviour, ICharacterPlatformStatus
{
    private IAgentController m_agentController;

    public bool IsOnPlatform { get; private set; }

    void Start()
    {
        m_agentController = GetComponent<IAgentController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -0.0f)
        {
            DetachFromPlatform();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Platform"))
        {
            AttachToPlatform(other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Platform"))
        {
            DetachFromPlatform();
        }
    }

    private void AttachToPlatform(Collider other)
    {
        if (!IsOnPlatform || transform.parent != other.transform)
        {
            transform.parent = other.transform;
            IsOnPlatform = true;
            m_agentController.SwitchMovement(false);
        }
    }

    private void DetachFromPlatform()
    {
        transform.parent = null;
        IsOnPlatform = false;
        m_agentController.SwitchMovement(true);
    }
}
