﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PushOtherAgent : MonoBehaviour
{
    [SerializeField]
    private float m_pushForce = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Pusher") /*&& !other.CompareTag("Player")*/)
        {
            var otherAgent = other.gameObject.GetComponent<NavMeshAgent>();
            if (otherAgent != null)
            {
                var agentController = other.gameObject.GetComponent<IAgentController>();

                if (agentController.IsEnabled && otherAgent.isActiveAndEnabled)
                {
                    var vec = otherAgent.transform.position - transform.position;
                    vec.y = 0.0f;
                    GameState.Instance.PushOtherAgent(otherAgent, vec.normalized * m_pushForce);
                }
            }
        }
    }

}
