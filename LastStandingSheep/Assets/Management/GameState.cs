﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameState : Singleton<GameState>
{
    #region Private fields

    private readonly List<GameObject> m_sheepList = new List<GameObject>();
    private GameObject m_player;
    private GameObject m_woolf;
    private GameObject m_platform;
    private GameObject m_ufo;
    private GameManager m_gameManager;

    #endregion

    #region Properties

    public List<GameObject> SheepList => m_sheepList;
    public GameObject Player => m_player;
    public GameObject Platform => m_platform;
    public GameObject Woolf => m_woolf;
    public GameObject Ufo => m_ufo;
    public float PlatformScale { get; set; } = 1.0f;
    public bool IsGameStarted { get; private set; }

    #endregion

    #region Initialization

    private GameState()
    {
    }

    public void Initialize(GameManager gameManager)
    {
        m_gameManager = gameManager;
    }

    #endregion

    #region Add/Remove

    public GameObject AddSheep()
    {
        var sheep = ObjectsManager.Instance.Get(m_gameManager.PrefabList.SheepBot);
        Helper.RandomizePositionAndOrientation(sheep, GameConstants.BotAppearRadius);
        SheepList.Add(sheep);
        return sheep;
    }

    public void RemoveSheep(GameObject sheep)
    {
        ObjectsManager.Instance.Put(sheep);
        m_sheepList.Remove(sheep);
    }

    public GameObject AddPlayer()
    {
        m_player = ObjectsManager.Instance.Get(m_gameManager.PrefabList.Player);
        Helper.RandomizePositionAndOrientation(m_player, GameConstants.BotAppearRadius);
        return m_player;
    }

    private void RemovePlayer(GameObject target)
    {
        ObjectsManager.Instance.Put(target);
        m_player = null;
    }

    public GameObject AddWoolf()
    {
        m_woolf = ObjectsManager.Instance.Get(ObjectsManager.Instance.PrefabList.WoolfBot);
        Helper.RandomizePositionAndOrientation(m_woolf, GameConstants.BotAppearRadius);
        return m_woolf;
    }

    public void RemoveWoolf(GameObject woolf)
    {
        ObjectsManager.Instance.Put(woolf);
        m_woolf = null;
    }

    public GameObject AddUfo()
    {
        m_ufo = ObjectsManager.Instance.Get(ObjectsManager.Instance.PrefabList.UfoBot);
        Helper.RandomizePositionAndOrientation(m_ufo, GameConstants.BotAppearRadius);
        return m_ufo;
    }

    public void RemoveUfo(GameObject woolf)
    {
        ObjectsManager.Instance.Put(m_ufo);
        m_ufo = null;
    }

    public GameObject AddPlatform()
    {
        m_platform = ObjectsManager.Instance.Get(ObjectsManager.Instance.PrefabList.Platform);
        Helper.RandomizePosition(m_platform, GameConstants.PlatformAppearRadius, -0.9f);
        //m_platform.transform.position = new Vector3(0, -0.9f, 0);
        return m_platform;
    }

    public void RemovePlatform(GameObject platform)
    {
        ObjectsManager.Instance.Put(platform);
        m_platform = null;
    }

    private void RemoveObject(GameObject target)
    {
        if (target.CompareTag("Player"))
            RemovePlayer(target);
        else
            RemoveSheep(target);
    }

    #endregion

    #region Effects

    public void KillObject(GameObject target)
    {
        GameObject prefab = null;
        if (target.CompareTag("Player"))
            prefab = ObjectsManager.Instance.PrefabList.KillPlayerEffect;
        else
            prefab = ObjectsManager.Instance.PrefabList.KillSheepEffect;

        var effect = ObjectsManager.Instance.Get(prefab);
        effect.transform.position = target.transform.position;
        effect.transform.rotation = target.transform.rotation;
        var action = effect.GetComponent<IAction>();
        if (action is IInitializable initializable)
            initializable.Initialize();
        action.DoAction();

        var completable = effect.GetComponent<ICompletable>();
        completable.OnComplete += OnEffectComplete;

        RemoveObject(target);

        if (target.CompareTag("Player"))
        {
            ShowMenu("You Lose");
        }
    }

    public ICompletable UfoAbductObject(GameObject source, GameObject target)
    {
        GameObject prefab = null;
        if (target.CompareTag("Player"))
            prefab = ObjectsManager.Instance.PrefabList.AbductPlayerEffect;
        else
            prefab = ObjectsManager.Instance.PrefabList.AbductSheepEffect;

        var effect = ObjectsManager.Instance.Get(prefab);
        effect.transform.position = target.transform.position;
        effect.transform.rotation = target.transform.rotation;
        var action = effect.GetComponent<IAction>();
        var abduct = effect.GetComponent<AbductEffect>();
        abduct.Source = source;
        ((IInitializable)action).Initialize();
        action.DoAction();

        var completable = effect.GetComponent<ICompletable>();
        completable.OnComplete += OnEffectComplete;

        RemoveObject(target);

        if (target.CompareTag("Player"))
        {
            ShowMenu("You Lose");
        }

        return completable;
    }

    private void ShowMenu(string title)
    {
        var gameObject = Object.Instantiate(m_gameManager.PrefabList.Menu);
        var menu = gameObject.GetComponent<Menu>();
        menu.SetTitle(title);
    }

    public void MakeCloud(GameObject target)
    {
        GameObject prefab = null;
        if (target.CompareTag("Player"))
            prefab = ObjectsManager.Instance.PrefabList.PlayerBecomeCloudEffect;
        else
            prefab = ObjectsManager.Instance.PrefabList.SheepBecomeCloudEffect;

        if (prefab != null)
        {
            var effect = ObjectsManager.Instance.Get(prefab);
            effect.transform.position = target.transform.position;
            effect.transform.rotation = target.transform.rotation;
            var action = effect.GetComponent<IAction>();
            ((IInitializable)action).Initialize();
            //action.DoAction();

            var completable = effect.GetComponent<ICompletable>();
            completable.OnComplete += OnBecomeCloud;
        }

        RemoveObject(target);

        if (target.CompareTag("Player"))
        {
            ShowMenu("You Lose");
        }
    }

    void OnEffectComplete(GameObject gameObject)
    {
        ObjectsManager.Instance.Put(gameObject);
    }

    private void OnBecomeCloud(GameObject gameObject)
    {
        ObjectsManager.Instance.Put(gameObject);
    }

    #endregion

    #region Game Flow Methods

    public void RestartGame()
    {
        ClearGame();
        StartNewGame();
    }

    public void ClearGame()
    {
        IsGameStarted = false;

        var gameEvents = m_gameManager.GetComponentsInChildren<GameEvent>();
        foreach (var gameEvent in gameEvents)
        {
            gameEvent.StopAllCoroutines();
        }
        foreach (var sheep in m_sheepList)
        {
            ObjectsManager.Instance.Put(sheep);
        }
        m_sheepList.Clear();

        if (m_player != null)
        {
            ObjectsManager.Instance.Put(m_player);
            m_player = null;
        }
        if (m_woolf != null)
        {
            ObjectsManager.Instance.Put(m_woolf);
            m_woolf = null;
        }
        if (m_platform != null)
        {
            ObjectsManager.Instance.Put(m_platform);
            m_platform = null;
        }
        if (m_ufo != null)
        {
            ObjectsManager.Instance.Put(m_ufo);
            m_ufo = null;
        }

        EnableGameManagerEvents(false);
    }

    public void StartNewGame()
    {
        PlatformScale = 1.0f;
        for (var i = 0; i < m_gameManager.SheepCountOnStart; ++i)
            AddSheep();

        AddPlayer();

        EnableGameManagerEvents(true);
        IsGameStarted = true;
    }

    private void EnableGameManagerEvents(bool enable)
    {
        for (var i = 0; i < m_gameManager.transform.childCount; ++i)
        {
            var child = m_gameManager.transform.GetChild(i);
            child.gameObject.SetActive(enable);
            var initializable = child.GetComponent<IInitializable>();
            if (enable)
                initializable?.Initialize();
        }
    }

    public void WinGame()
    {
        ClearGame();

        ShowMenu("You Win");
    }

    #endregion

    #region Push agent

    public void PushOtherAgent(NavMeshAgent otherAgent, Vector3 vec)
    {
        m_gameManager.StartCoroutine(PushAgent(otherAgent, vec));
    }

    private IEnumerator PushAgent(NavMeshAgent otherAgent, Vector3 vec)
    {
        var startTime = Time.time;
        var pushTime = 0.5f;

        if (otherAgent == null || otherAgent.Equals(null))
            yield break;

        var agentController = otherAgent.GetComponent<IAgentController>();

        agentController.SwitchMovement(false);
        var active = true;

        var startPosition = otherAgent.transform.position;
        var endPosition = startPosition + vec;


        while (active)
        {
            if (otherAgent == null || otherAgent.Equals(null))
                yield break;

            var deltaTime = Time.time - startTime;
            var t = deltaTime / pushTime;
            if (t > 1.0f)
            {
                t = 1.0f;
                active = false;
            }

            if (otherAgent.transform.position.magnitude > GameConstants.LevelRadius)
            {
                Instance.MakeCloud(otherAgent.gameObject);
            }

            otherAgent.transform.position = Vector3.Lerp(startPosition, endPosition, t);

            yield return null;
        }
        agentController.SwitchMovement(true);
    }

    #endregion
}