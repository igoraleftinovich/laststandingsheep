﻿using System;
using UnityEngine;

public interface ICompletable
{
    Action<GameObject> OnComplete { get; set; }
}