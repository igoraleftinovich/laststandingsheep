﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int m_sheepCountOnStart = 10;

    [SerializeField] private PrefabList m_prefabList;

    public int SheepCountOnStart => m_sheepCountOnStart;

    public PrefabList PrefabList => m_prefabList;

    void Start()
    {
        ObjectsManager.Instance.Initialize(m_prefabList);
        GameState.Instance.Initialize(this);

        GameState.Instance.StartNewGame();
    }

    void Update()
    {
        if (GameState.Instance.IsGameStarted)
        {
            if (GameState.Instance.SheepList.Count == 0 && GameState.Instance.Woolf == null)
            {
                GameState.Instance.WinGame();
            }
        }
    }
}