﻿public static class GameConstants
{
    public const float BotAppearRadius = 9.0f;
    public const float LevelRadius = 10.0f;
    public const float PlatformAppearRadius = 6.0f;
}