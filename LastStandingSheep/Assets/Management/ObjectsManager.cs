﻿using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class ObjectsManager : Singleton<ObjectsManager>
{
    public PrefabList PrefabList { get; private set; }

    private ObjectsManager()
    {
    }

    public GameObject Get(GameObject prefab)
    {
        var instance = Object.Instantiate(prefab);
        return instance;
    }

    public void Put(GameObject obj)
    {
        Object.Destroy(obj);
    }

    public void Initialize(PrefabList prefabList)
    {
        PrefabList = prefabList;
    }
}