﻿using System;
using UnityEngine;

[Serializable]
public struct PrefabList
{
    public GameObject Player;
    public GameObject SheepBot;
    public GameObject WoolfBot;
    public GameObject UfoBot;
    public GameObject Platform;
    public GameObject KillSheepEffect;
    public GameObject KillPlayerEffect;
    public GameObject AbductPlayerEffect;
    public GameObject AbductSheepEffect;
    public GameObject SheepBecomeCloudEffect;
    public GameObject PlayerBecomeCloudEffect;
    public GameObject Menu;
}