﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class Helper
{
    public static void RandomizePositionAndOrientation(GameObject gameObject, float radius)
    {
        RandomizePosition(gameObject, radius);
        gameObject.transform.rotation = Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f);
    }

    public static void RandomizePosition(GameObject gameObject, float radius)
    {
        RandomizePosition(gameObject, radius, 0.0f);
    }

    public static void RandomizePosition(GameObject gameObject, float radius, float y)
    {
        gameObject.transform.position = GetRandomPosition(radius, y);
    }

    public static Vector3 GetRandomPosition(float radius, float y = 0.0f)
    {
        var rndPos = Random.insideUnitCircle * radius;
        return new Vector3(rndPos.x, y, rndPos.y);
    }

    public static bool IsCharacterOnPlatform(GameObject gameObject)
    {
        if (gameObject == null)
            return false;

        var status = gameObject.GetComponent<ICharacterPlatformStatus>();
        return (!status.IsOnPlatform);
    }


    public static GameObject FindTarget()
    {
        var targetList = new List<GameObject>();
        foreach (var sheep in GameState.Instance.SheepList)
        {
            if (Helper.IsCharacterOnPlatform(sheep))
                targetList.Add(sheep);
        }

        if (Helper.IsCharacterOnPlatform(GameState.Instance.Player))
            targetList.Add(GameState.Instance.Player);

        if (targetList.Count > 0)
        {
            var sheepIndex = Random.Range(0, targetList.Count);
            return targetList[sheepIndex];
        }

        return null;
    }

    public static bool IsAgentCompleteMove(NavMeshAgent agent)
    {
        if (!agent.isActiveAndEnabled)
            return false;

        return !float.IsPositiveInfinity(agent.remainingDistance) &&
               agent.pathStatus == NavMeshPathStatus.PathComplete &&
               agent.remainingDistance <= agent.stoppingDistance;
    }
}