﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDestinationInput
{
    Vector3 DestinationInput { get; }
}
