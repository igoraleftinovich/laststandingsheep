﻿using UnityEngine;

public abstract class MonoAction : MonoBehaviour, IAction, IInitializable
{
    public abstract void Initialize();
    public abstract bool DoAction();
}