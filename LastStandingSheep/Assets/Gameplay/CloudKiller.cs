﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudKiller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Woolf") || other.CompareTag("Sheep"))
            GameState.Instance.MakeCloud(other.gameObject);
    }
}
