﻿using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour, IInitializable
{
    private ICondition m_condition;
    private IAction[] m_actions;

    private bool m_isActive;
    private List<IAction> m_activeActions = new List<IAction>();

    void Start()
    {
        m_condition = GetComponent<ICondition>();
        m_actions = GetComponents<IAction>();

        Initialize();
    }

    public void Initialize()
    {
        m_isActive = false;
        ((IInitializable)m_condition).Initialize();
    }

    void Update()
    {
        if (!m_isActive)
        {
            if (m_condition.Check())
            {
                m_activeActions = new List<IAction>(m_actions);

                foreach (var action in m_activeActions)
                {
                    ((IInitializable)action).Initialize();
                }

                m_isActive = true;
            }
        }
        else
        {
            var actionsToRemove = new List<IAction>();
            foreach (var activeAction in m_activeActions)
            {
                if (activeAction.DoAction())
                {
                    actionsToRemove.Add(activeAction);
                    ((IInitializable)m_condition).Initialize();
                }
            }

            foreach (var action in actionsToRemove)
            {
                m_activeActions.Remove(action);
            }

            if (m_activeActions.Count == 0)
            {
                m_isActive = false;
            }
        }
    }
}