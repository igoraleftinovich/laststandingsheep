﻿public interface ICharacterPlatformStatus
{
    bool IsOnPlatform { get; }
}