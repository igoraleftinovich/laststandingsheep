﻿using UnityEngine;

public class Platform : MonoBehaviour
{
    private Collider m_collider;

    void Start()
    {
        m_collider = GetComponent<Collider>();
    }
    public void SwitchTrigger(bool val)
    {
        m_collider.enabled = val;
    }
}