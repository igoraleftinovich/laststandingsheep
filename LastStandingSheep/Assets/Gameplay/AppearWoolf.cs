﻿using System.Collections;
using UnityEngine;

public class AppearWoolf : CoroutineAction
{
    private GameObject m_woolf;

    [SerializeField]
    private float m_visibleTime = 10.0f;


    public override void Initialize()
    {
        base.Initialize();
    }

    protected override void CompleteAction()
    {
        GameState.Instance.RemoveWoolf(m_woolf);
    }


    protected override IEnumerator ActionLogic()
    {
        m_woolf = GameState.Instance.AddWoolf();

        yield return new WaitForSeconds(m_visibleTime);
    }

}