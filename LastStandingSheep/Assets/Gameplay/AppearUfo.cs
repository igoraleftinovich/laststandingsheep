﻿using System.Collections;
using UnityEngine;

public class AppearUfo : CoroutineAction
{
    private GameObject m_ufo;

    [SerializeField]
    private float m_visibleTime = 10.0f;


    public override void Initialize()
    {
        base.Initialize();
    }

    protected override void CompleteAction()
    {
        GameState.Instance.RemoveUfo(m_ufo);
    }


    protected override IEnumerator ActionLogic()
    {
        m_ufo = GameState.Instance.AddUfo();
        var abductionState = m_ufo.GetComponentInChildren<IAbductionState>();
        yield return new WaitForSeconds(m_visibleTime);

        while (abductionState.IsAbductionInProcess)
            yield return null;
    }

}