﻿using UnityEngine;
using UnityEngine.AI;

public class AgentMotor : MonoBehaviour, IAgentController
{
    #region References

    private NavMeshAgent m_agent;
    private IDestinationInput m_destinationInput;
    private IMoveSpeedInput m_moveSpeedInput;

    public bool DisableMovement;
    public bool IsEnabled => !DisableMovement && m_agent != null && m_agent.isActiveAndEnabled;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        m_agent = GetComponent<NavMeshAgent>();
        m_destinationInput = GetComponentInChildren<IDestinationInput>();
        m_moveSpeedInput = GetComponentInChildren<IMoveSpeedInput>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_agent.isActiveAndEnabled)
            return;
        if (DisableMovement)
            return;

        var vecToDestination = m_destinationInput.DestinationInput - transform.position;
        vecToDestination.y = 0;

        if (vecToDestination.magnitude > 0.1f)
        {
            var moveSpeed = m_moveSpeedInput?.MoveSpeed ?? 1.0f;
            m_agent.speed = moveSpeed;
            m_agent.SetDestination(m_destinationInput.DestinationInput);
        }

    }

    public void SwitchMovement(bool val)
    {
        if (val)
        {
            m_agent.enabled = true;
            m_agent.isStopped = false;
            m_agent.updatePosition = true;
            m_agent.velocity = Vector3.zero;
        }
        else
        {
            if (m_agent.isActiveAndEnabled)
            {
                m_agent.ResetPath();
                m_agent.isStopped = true;
                m_agent.updatePosition = false;
                m_agent.enabled = false;
            }
        }
        DisableMovement = !val;
    }

}

public interface IAgentController
{
    void SwitchMovement(bool val);
    bool IsEnabled { get; }
}