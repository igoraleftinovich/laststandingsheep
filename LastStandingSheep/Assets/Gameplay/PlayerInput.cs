﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour, IDestinationInput, IMoveSpeedInput
{
    private Transform m_characterTransform;

    [SerializeField]
    private float m_moveSpeed = 2.0f;

    private IAgentController m_agentController;

    public Vector3 DestinationInput { get; private set; }
    public float MoveSpeed => m_moveSpeed;

    // Start is called before the first frame update
    void Start()
    {
        m_characterTransform = transform.parent;
        m_agentController = m_characterTransform.GetComponent<IAgentController>();

        DestinationInput = m_characterTransform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_agentController.IsEnabled)
        {
            foreach (Touch touch in Input.touches)
            {
                HandleTouch(touch.fingerId, touch.position, touch.phase);
            }

            // Simulate touch events from mouse events
            if (Input.touchCount == 0)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    HandleTouch(10, Input.mousePosition, TouchPhase.Began);
                }

                if (Input.GetMouseButton(0))
                {
                    HandleTouch(10, Input.mousePosition, TouchPhase.Moved);
                }

                if (Input.GetMouseButtonUp(0))
                {
                    HandleTouch(10, Input.mousePosition, TouchPhase.Ended);
                }
            }
        }
        else
        {
            DestinationInput = m_characterTransform.position;

        }
    }

    private void HandleTouch(int touchFingerId, Vector3 touchPosition, TouchPhase touchPhase)
    {
        if (touchPhase == TouchPhase.Began || touchPhase == TouchPhase.Moved)
        {
            var ray = Camera.main.ScreenPointToRay(touchPosition);
            if (Physics.Raycast(ray, out var hit))
            { 
                DestinationInput = hit.point;
            }
        }
    }

}