﻿using UnityEngine;

public interface IAnimatorStateExitHandler
{
    void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);
}