﻿using UnityEngine;

public abstract class MonoCondition : MonoBehaviour, ICondition, IInitializable
{
    public abstract void Initialize();
    public abstract bool Check();
}