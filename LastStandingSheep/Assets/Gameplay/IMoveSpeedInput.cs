﻿using UnityEngine;

public interface IMoveSpeedInput
{
    float MoveSpeed { get; }
}