﻿using UnityEngine;

public class RandomTimeCondition : MonoCondition
{
    [SerializeField] private float m_timeMin = 5;
    [SerializeField] private float m_timeMax = 10;


    private float m_waitTime;
    private float m_startTime;

    public override void Initialize()
    {
        m_startTime = Time.time;
        m_waitTime = Random.Range(m_timeMin, m_timeMax);
    }

    public override bool Check()
    {
        return Time.time - m_startTime > m_waitTime;
    }
}