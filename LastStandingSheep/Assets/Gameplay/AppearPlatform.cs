﻿using System.Collections;
using UnityEngine;

public class AppearPlatform : CoroutineAction
{
    [SerializeField]
    private float m_highlightTime = 3.0f;
    [SerializeField]
    private float m_liftUpTime = 2.0f;
    [SerializeField]
    private float m_visibleTime = 10.0f;

    private GameObject m_platform;

    public override void Initialize()
    {
        m_platform = GameState.Instance.AddPlatform();
        m_platform.transform.localScale = new Vector3(GameState.Instance.PlatformScale, 1.0f, GameState.Instance.PlatformScale);

        base.Initialize();
    }

    protected override void CompleteAction()
    {
        GameState.Instance.RemovePlatform(m_platform);
        m_platform = null;

        if (GameState.Instance.PlatformScale > 0.25f)
            GameState.Instance.PlatformScale -= 0.25f;  
    }

    protected override IEnumerator ActionLogic()
    {
        yield return HighlightPlatform();
        yield return LiftUp();
        yield return ShowPlatform();
        yield return LiftDown();
    }

    private IEnumerator ShowPlatform()
    {
        yield return new WaitForSeconds(m_visibleTime);
    }

    IEnumerator HighlightPlatform()
    {
        yield return new WaitForSeconds(m_highlightTime);
    }

    IEnumerator LiftUp()
    {
        var startTime = Time.time;

        var startPosition = m_platform.transform.position;
        var endPosition = new Vector3(startPosition.x, 0.0f, startPosition.z);
        var platform = m_platform.GetComponent<Platform>();
        platform.SwitchTrigger(true);

        while (Time.time - startTime < m_liftUpTime)
        {
            var deltaTime = Time.time - startTime;
            var k = deltaTime / m_liftUpTime;

            m_platform.transform.position = Vector3.Lerp(startPosition, endPosition, k);
            yield return null;
        }
    }

    IEnumerator LiftDown()
    {
        var startTime = Time.time;

        var startPosition = m_platform.transform.position;
        var endPosition = new Vector3(startPosition.x, -2.1f, startPosition.z);

        while (Time.time - startTime < m_liftUpTime)
        {
            var deltaTime = Time.time - startTime;
            var k = deltaTime / m_liftUpTime;

            m_platform.transform.position = Vector3.Lerp(startPosition, endPosition, k);
            yield return null;
        }
    }


}