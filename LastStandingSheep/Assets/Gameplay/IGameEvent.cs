﻿using System.Collections;
using System.Linq;

public interface IGameEvent
{
    void Process();
}

internal interface ICondition
{
    bool Check();
}

public interface IAction
{
    bool DoAction();
}