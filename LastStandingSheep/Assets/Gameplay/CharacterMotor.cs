﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour
{
    #region References

    private CharacterController m_characterController;
    private IDestinationInput m_destinationInput;
    private IMoveSpeedInput m_moveSpeedInput;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        m_characterController = GetComponent<CharacterController>();
        m_destinationInput = GetComponentInChildren<IDestinationInput>();
        m_moveSpeedInput = GetComponentInChildren<IMoveSpeedInput>();
    }

    // Update is called once per frame
    void Update()
    {

        var vecToDestination = m_destinationInput.DestinationInput - transform.position;
        vecToDestination.y = 0;

        if (vecToDestination.magnitude > 0.1f)
        {
            var moveSpeed = m_moveSpeedInput?.MoveSpeed ?? 1.0f;

            var newDirection = Vector3.RotateTowards(transform.forward, vecToDestination.normalized, 10 * Time.fixedDeltaTime, 0.0f);
            var targetRotation = Quaternion.LookRotation(newDirection, Vector3.up);

            m_characterController.SimpleMove(vecToDestination.normalized * moveSpeed);
            transform.rotation = targetRotation;
        }

    }

}