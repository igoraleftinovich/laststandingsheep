﻿using System;
using System.Collections;
using UnityEngine;

public abstract class CoroutineAction : MonoAction, ICompletable
{
    [SerializeField]
    private float m_delayTime = 0.0f;

    private bool m_isActive;

    public Action<GameObject> OnComplete { get; set; }

    public override bool DoAction()
    {
        return !m_isActive;
    }

    public override void Initialize()
    {
        StartCoroutine(ActionLogicInternal());
    }

    protected abstract void CompleteAction();

    private IEnumerator ActionLogicInternal()
    {
        m_isActive = true;
        yield return new WaitForSeconds(m_delayTime);
        yield return ActionLogic();
        CompleteAction();
        OnComplete?.Invoke(gameObject);
        m_isActive = false;
    }

    protected abstract IEnumerator ActionLogic();

}