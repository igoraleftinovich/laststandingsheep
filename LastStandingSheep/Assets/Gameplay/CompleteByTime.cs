﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteByTime : MonoBehaviour, ICompletable
{
    [SerializeField] private float m_time = 3;

    public Action<GameObject> OnComplete { get; set; }

    void Awake()
    {
        StartCoroutine(WaitForTime());
    }

    private IEnumerator WaitForTime()
    {
        yield return new WaitForSeconds(m_time);

        OnComplete?.Invoke(gameObject);
    }
}
