﻿using System;
using UnityEngine;
using UnityEngine.AI;

public enum BotState
{
    Patrol,
    EscapeWoolf,
    MoveToPlatform,
}

public class BotInput : MonoBehaviour, IDestinationInput, IMoveSpeedInput
{
    private Transform m_characterTransform;
    private NavMeshAgent m_agent;

    [SerializeField]
    private float m_patrolSpeed = 0.5f;
    [SerializeField]
    private float m_moveToPlatformSpeed = 0.8f;
    [SerializeField]
    private float m_escapeSpeed = 1.2f;

    private BotState m_botState;

    public Vector3 DestinationInput { get; private set; }
    public float MoveSpeed { get; private set; }


    // Start is called before the first frame update
    void Start()
    {
        m_characterTransform = transform.parent;

        m_agent = m_characterTransform.GetComponent<NavMeshAgent>();
        DestinationInput = m_characterTransform.position;
    }

    void Update()
    {
        if (!m_agent.isActiveAndEnabled)
            return;

        if (GameState.Instance.Woolf != null)
        {
            if (m_botState != BotState.EscapeWoolf)
                m_botState = BotState.EscapeWoolf;
        }
        else if (GameState.Instance.Platform != null)
        {
            if (m_botState != BotState.MoveToPlatform)
                m_botState = BotState.MoveToPlatform;
        }
        else
        {
            if (m_botState != BotState.Patrol)
            {
                DestinationInput = Helper.GetRandomPosition(GameConstants.BotAppearRadius);
                m_botState = BotState.Patrol;
            }
        }

        switch (m_botState)
        {
            case BotState.Patrol:
                Patrol();
                break;
            case BotState.EscapeWoolf:
                EscapeFromWoolf(GameState.Instance.Woolf);
                break;
            case BotState.MoveToPlatform:
                MoveToPlatform(GameState.Instance.Platform);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void Patrol()
    {
        MoveSpeed = m_patrolSpeed;
        if (Helper.IsAgentCompleteMove(m_agent))
        {
            DestinationInput = Helper.GetRandomPosition(GameConstants.BotAppearRadius);
        }
    }

    private void MoveToPlatform(GameObject platform)
    {
        MoveSpeed = m_moveToPlatformSpeed;
        DestinationInput = new Vector3(platform.transform.position.x, 0, platform.transform.position.z);
    }

    private void EscapeFromWoolf(GameObject woolf)
    {
        MoveSpeed = m_escapeSpeed;
        var vecFromTarget = m_characterTransform.position - woolf.transform.position;
        vecFromTarget.y = 0;
        DestinationInput = m_characterTransform.position + vecFromTarget.normalized;
    }

}