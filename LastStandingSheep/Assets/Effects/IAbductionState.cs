﻿public interface IAbductionState
{
    bool IsAbductionInProcess { get; }
}