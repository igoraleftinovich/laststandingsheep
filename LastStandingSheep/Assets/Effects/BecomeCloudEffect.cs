﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BecomeCloudEffect : CoroutineAction, IAnimatorStateExitHandler
{
    private Animator m_animator;
    private bool m_isAnimationComplete;

    void Start()
    {
        m_animator = GetComponent<Animator>();
    }


    public override void Initialize()
    {

        base.Initialize();
    }

    protected override void CompleteAction()
    {
        ObjectsManager.Instance.Put(gameObject);
    }

    protected override IEnumerator ActionLogic()
    {
        m_isAnimationComplete = false;
        m_animator.SetTrigger("MakeCloud");

        while (!m_isAnimationComplete)
            yield return null;
    }

    private IEnumerator WaitForAnimation(Animation animation)
    {
        do
        {
            yield return null;
        } while (animation.isPlaying);
    }

    public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName("CloudGrowth"))
        {
            m_isAnimationComplete = true;
        }
    }
}
