﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddExplosionForceToChildren : MonoBehaviour, IAction
{
    private Rigidbody[] m_rigidBodies;

    [SerializeField]
    private float m_explosionForce = 10;
    [SerializeField]
    private float m_explosionRadius = 2;
    [SerializeField]
    private float m_upwardsModifier = 3.0f;

    void Awake()
    {
        m_rigidBodies = GetComponentsInChildren<Rigidbody>();
    }

    public bool DoAction()
    {
        StartCoroutine(AddForceAfterDelay());
        return true;
    }

    private IEnumerator AddForceAfterDelay()
    {
        yield return new WaitForFixedUpdate();

        foreach (var rigidBody in m_rigidBodies)
        {
            rigidBody.AddExplosionForce(m_explosionForce, transform.position + Vector3.up * 0.3f, m_explosionRadius, m_upwardsModifier);
        }
    }
}
