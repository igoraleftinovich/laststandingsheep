﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbductEffect : CoroutineAction
{
    [SerializeField]
    private float m_effectTime = 2.0f;

    private bool m_isComplete;
    public GameObject Source { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Awake()
    {
        m_isComplete = false;
    }

    protected override void CompleteAction()
    {
        Destroy(gameObject);
    }

    protected override IEnumerator ActionLogic()
    {
        var startPosition = transform.position;
        var startTime = Time.time;

        while (!m_isComplete)
        {
            if (Source == null || Source.Equals(null))
                yield break;
            
            var t = (Time.time - startTime) / m_effectTime;
            if (t > 1.0f)
            {
                t = 1.0f;
                m_isComplete = true;
            }
            transform.position = Vector3.Lerp(startPosition, Source.transform.position, t);

            yield return null;
        }
    }
}
