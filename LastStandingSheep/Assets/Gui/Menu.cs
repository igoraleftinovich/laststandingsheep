﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField]
    private Text m_titleText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnRestartGameClick()
    {
        GameState.Instance.RestartGame();
        Destroy(gameObject);
    }

    public void SetTitle(string title)
    {
        m_titleText.text = title;
    }
}
